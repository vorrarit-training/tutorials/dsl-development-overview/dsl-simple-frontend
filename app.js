const API_BASE_URL = "https://frontend.lab.dsltraining.net";
var text1 = document.getElementById('text1');
var button1 = document.getElementById('button1');
var label1 = document.getElementById('label1');

button1.onclick = function() {
    label1.innerHTML = 'Empty...';

    var oReq = new XMLHttpRequest();
    oReq.responseType = 'json';
    oReq.onreadystatechange = function() {
        if (oReq.readyState == XMLHttpRequest.DONE) {
            var account = oReq.response;
            label1.innerHTML = 'Account No: ' + account.accountNo + '<br>' +
                    'Name: ' + account.firstName + ' ' + account.lastName + '<br>' +
                    'Loan Amount: ' + account.loanAmount;
        }
    }

    oReq.open("GET", API_BASE_URL + "/api/dsl-simple-backend/account/info/" + text1.value + "/1");
    oReq.send();
}
